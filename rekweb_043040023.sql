-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 17, 2018 at 07:13 PM
-- Server version: 5.6.33
-- PHP Version: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rekweb_043040023`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(3) NOT NULL,
  `cover` varchar(150) DEFAULT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `sinopsis` varchar(100) NOT NULL,
  `pengarang` varchar(25) DEFAULT NULL,
  `penerbit` varchar(40) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  `tahun_terbit` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id_buku`, `cover`, `judul`, `sinopsis`, `pengarang`, `penerbit`, `id_kategori`, `isbn`, `tahun_terbit`) VALUES
(1, 'https://ssvr.bukukita.com/babacms/displaybuku/104214_f.jpg', 'Hiu & Buaya', '', 'Dhety Azmi', 'Grass Media', 3, '9789797945497', 2018),
(2, 'https://ssvr.bukukita.com/babacms/displaybuku/3343.jpg', 'Laskar Pelangi', '', 'Andrea Hinata', 'Bentang Pustaka', 4, '97893062792', 2005),
(3, 'https://ssvr.bukukita.com/babacms/displaybuku/89373_f.jpg', 'Ayat-Ayat Cinta 2', '', 'Habiburrahman El Shirazy', 'Republika', 4, '9786020822150', 2015),
(4, 'z', 'z', '', 'z', 'z', 2, '123', 2011),
(6, 'kaka', 'kakak', '', 'kakakak', 'zazazazaza', 5, '9999', 2012);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Fiksi'),
(2, 'Misteri'),
(3, 'Komedi'),
(4, 'Novel'),
(5, 'Biografi');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nrp` char(9) NOT NULL,
  `nama` varchar(254) NOT NULL,
  `email` varchar(254) NOT NULL,
  `jurusan` varchar(254) NOT NULL,
  `gambar` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nrp`, `nama`, `email`, `jurusan`, `gambar`) VALUES
(21, '043040023', 'Sandhika Galih', 'sandhika@unpas.ac.id', 'Teknik Informatika', 'sandhika.jpeg'),
(22, '033040123', 'Erik', 'erik@gmail.com', 'Teknik Mesin', 'erik.jpg'),
(23, '023040012', 'Fajar Darmawan ', 'fajar@yahoo.com', 'Teknik Informatika', 'fajar.jpg'),
(25, '023040001', 'Anggoro Ari', 'anggoro@unpas.ac.id', 'Teknik Informatika', 'tes.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nrp` (`nrp`),
  ADD KEY `nama` (`nama`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

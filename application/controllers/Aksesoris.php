<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aksesoris extends CI_Controller {
	public function index()
	{	
		$data['judul'] = 'Halaman Daftar Aksesoris'; 
        // jika ada inputan dari form maka masuk ke dalam if
		if( $this->input->post() ) { // ini adalah menangkap ada tidaknya aksi dari form
			// menangkap isian dari input text di form
			$keyword = $this->input->post('keyword');
			$array = [
				'nama' => $keyword,
				'stok' => $keyword,
				'harga' => $keyword
			];
			$this->db->or_like($array);
		} 
		$query = $this->db->get('aksesoris');
		$data['aksesoris'] = $query->result_array();

		$this->load->view('templates/header', $data);
		$this->load->view('aksesoris/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() {
		$data['judul'] = 'Tambah Data Aksesoris';

		if( $this->input->post() ) {
			
			$aks['nama'] = $this->input->post('nama');
			$aks['jenis'] = $this->input->post('jenis');
			$aks['stok'] = $this->input->post('stok');
			$aks['harga'] = $this->input->post('harga');
			$aks['gambar'] = $this->input->post('gambar');
			$this->load->model('aksesoris_model');

			if( $this->aksesoris_model->tambah($aks) > 0 ) {
				$this->session->set_flashdata('info_tambah', 'data berhasil ditambahkan!');
			}
			redirect('aksesoris');
		}

		$this->load->view('templates/header', $data);
		$this->load->view('aksesoris/tambah', $data);
		$this->load->view('templates/footer');
	}


	public function hapus($id)
	{
		$this->load->model('aksesoris_model');

		if( $this->aksesoris_model->hapus($id) > 0 ) {
			$this->session->set_flashdata('info_hapus', 'data berhasil dihapus!');
		}

		redirect('aksesoris');
	}


	public function ubah($id)
	{
		$data['judul'] = 'Ubah Data Aksesoris';

		$this->load->model('aksesoris_model');
		$data['aks'] = $this->aksesoris_model->get_mhs_by_id($id);

		// cek apakah tombol ubah sudah ditekan
		if( $this->input->post() ) {
			$aks['nama'] = $this->input->post('nama');
			$aks['jenis'] = $this->input->post('jenis');
			$aks['stok'] = $this->input->post('stok');
			$aks['harga'] = $this->input->post('harga');
			$aks['gambar'] = $this->input->post('gambar');
			$aks['id'] = $id;

			if( $this->aksesoris_model->ubah($aks) > 0 ) {
				$this->session->set_flashdata('info_ubah', 'data berhasil diubah!');
			}

			redirect('aksesoris');
		}

		$this->load->view('templates/header', $data);
		$this->load->view('aksesoris/ubah', $data);
		$this->load->view('templates/footer');
	}






}

<div class="container">
	<div class="row">
		<div class="col">
			<h1>Daftar Aksesoris</h1>
			<a href="<?= base_url('aksesoris/tambah'); ?>" class="btn btn-primary">Tambah Data Aksesoris</a>
			<br><br>

			<?php if( $this->session->flashdata('info_tambah') ) : ?>
				<div class="alert alert-success" role="alert">
				  <?= $this->session->flashdata('info_tambah'); ?>
				</div>
			<?php endif; ?>

			<?php if( $this->session->flashdata('info_hapus') ) : ?>
				<div class="alert alert-success" role="alert">
				  <?= $this->session->flashdata('info_hapus'); ?>
				</div>
			<?php endif; ?>

			<?= form_open('aksesoris/index'); ?>
				<div class="row">
					<div class="col-lg-6">
					    <div class="input-group">
					      <input type="text" class="form-control" name="keyword">
					      <span class="input-group-btn">
					        <button class="btn btn-primary" type="submit">Cari!</button>
					      </span>
					    </div>
					  </div>
				</div>
			</form>

			<br>

			<table class="table table-hover">
				<thead>
					<tr>
						<th>No.</th>
						<th>Aksi</th>
						<th>Gambar</th>
						<th>Nama</th>
						<th>Jenis</th>
						<th>Stok</th>
						<th>Harga</th>
						
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; ?>
					<?php foreach( $aksesoris as $row ) : ?>
					<tr>
						<td><?= $i++; ?></td>
						<td>
							<a href="<?= base_url('aksesoris/ubah/' . $row['id']); ?>" class="btn btn-info btn-sm">ubah</a>
							<a href="<?= base_url('aksesoris/hapus/' . $row['id']); ?>" onclick="return confirm('yakin?');" class="btn btn-danger btn-sm">hapus</a>
						</td>
						<td><img src="<?= base_url('assets/img/aksesoris/') . $row['gambar']; ?>" width="60" class="rounded-circle"></td>
						<td><?= $row['nama']; ?></td>
						<td><?= $row['jenis']; ?></td>
						<td><?= $row['stok']; ?></td>
						<td><?= $row['harga']; ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
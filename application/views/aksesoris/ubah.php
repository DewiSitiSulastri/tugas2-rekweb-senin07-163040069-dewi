<div class="container">
	
	<div class="row">
		<div class="col">
			<h1>Ubah Data Aksesoris</h1>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<?= form_open('aksesoris/ubah/' . $aks['id']); ?>
				<div class="form-group row">
			      <label for="nama" class="col-sm-2 col-form-label">Nama</label>
			      <div class="col-sm-10">
			        <input type="text" class="form-control" id="nama" name="nama" value="<?= $aks["nama"]; ?>">
			      </div>
			    </div>

			    
			    <div class="form-group row">
			      <label for="jenis" class="col-sm-2 col-form-label">Jenis</label>
			      <div class="col-sm-10">
			        <input type="text" class="form-control" id="jenis" name="jenis" value="<?= $aks["jenis"]; ?>">
			      </div>
			    </div>

			    <div class="form-group row">
			      <label for="stok" class="col-sm-2 col-form-label">Stok</label>
			      <div class="col-sm-10">
			        <input type="text" class="form-control" id="stok" name="stok" value="<?= $aks["stok"]; ?>">
			      </div>
			    </div>
			    <div class="form-group row">
			      <label for="harga" class="col-sm-2 col-form-label">Harga</label>
			      <div class="col-sm-10">
			        <input type="text" class="form-control" id="harga" name="harga" value="<?= $aks["harga"]; ?>">
			      </div>
			    </div>

			    <div class="form-group row">
			      <label for="gambar" class="col-sm-2 col-form-label">Gambar</label>
			      <div class="col-sm-10">
			        <input type="text" class="form-control" id="gambar" name="gambar" value="<?= $aks["gambar"]; ?>">
			      </div>
			    </div>

			    <button type="submit" class="btn btn-success">Ubah Data Aksesoris</button>
			</form>			
		</div>
	</div>







</div>
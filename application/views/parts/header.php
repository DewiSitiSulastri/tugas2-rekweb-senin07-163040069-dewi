<!DOCTYPE html>
<html>
<head>
	<title>BooKooKoo | <?= $title ?></title>

	<script src="<?=base_url('assets/js/jquery-3.1.0.min.js')?>"></script>
	<script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/js/material.min.js')?>"></script>

	<script src="<?=base_url('assets/js/material-dashboard.js')?>"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/bootstrap.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/material-dashboard.css')?>">
</head>
<body>
	<div class="wrapper">
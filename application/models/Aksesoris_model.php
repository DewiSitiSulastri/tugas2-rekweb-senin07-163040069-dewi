<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aksesoris_model extends CI_Model {

	public function tambah($aks) 
    {
		return $this->db->insert('aksesoris', $aks);
	}

    public function hapus($id)
    {
        return $this->db->where('id', $id)->delete('aksesoris');
    }

    public function get_aks_by_id($id)
    {
        $query = $this->db->get_where('aksesoris', ['id' => $id]);
        return $query->row_array();
    }

    public function ubah($aks)
    {
        $this->db->where('id', $aks['id']);
        return $this->db->update('aksesoris', $aks);
    }











}
